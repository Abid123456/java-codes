import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in);
        
		System.out.print("Enter username: ");
		String username = scanner.next();

		System.out.print("Enter password: ");
		String password = scanner.next();
		
		Bank bank = new Bank();
		bank.userLogin(username, password);
		bank.deposit(500);
		bank.withdraw(200);
		System.out.println(bank);
	}
}
