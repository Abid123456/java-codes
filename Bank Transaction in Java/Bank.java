import java.util.Objects;

public class Bank {
	
	private boolean isValidLogin;
	private int amount = 1000;
	private String userName = "Abid";
	private String password = "123";
	private String cnic = "396164-4613151-7";
	private String address = "Home";
	private String email = "abidshafique504@gmail.com";
	
	//Default constructor
	public Bank() {
		this("Abid", "123", "396164-4613151-7", "Home", "abidshafique504@gmail.com");
	}
	
	public Bank(String username, String password, String cnic, String address, String email) {
		this(username, password, 1000, cnic, address, email);
	}
	
	public Bank(String username, String password, int ammount, String mCnic, String mAddress, String mEmail) {
		this.userName = username;
		this.password = password;
		this.amount = ammount;
		this.cnic = mCnic;
		this.address = mAddress;
		this.email = mEmail;
	}
	
	public void userLogin(String userName , String password) {
		
		if(Objects.isNull(userName) || Objects.isNull(password) 
				|| userName.isEmpty() || password.isEmpty())
		{
			System.err.println("invalid data Malfunction [404]");
			return;
		}	
		
		if (this.userName.equals(userName) && this.password.equals(password)) {
			isValidLogin = true;
		} else {
			System.err.println("invalid credentials Malfunction [404]");
		}
		
	}
	
	public void deposit(int ammount) {
		if(isValidLogin) {
			if(ammount <= 0 ) {
				System.err.println("Invalid ammount to deposit [302]");
				return;
			}
			this.amount += ammount;
			System.out.println("Deposit successfully your current amount is "+this.amount);
		}
	}
	
	public void withdraw(int ammount) {
		if(isValidLogin) {
			if(ammount < 0 || ammount > this.amount) {
				System.err.println("Invalid ammount to withdraw [302]");
				return;
			}
			this.amount -= ammount;
			System.out.println("Withdraw successfully your current ammount is "+this.amount);
		}
	}
	
//	public int getAmmount() {
//		return amount;
//	}
//	
//	public void setAmmount(int ammount) {
//		this.amount = ammount;
//	}
//	
//	public String getUserName() {
//		return userName;
//	}
//	
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//	
//	public String getPassword() {
//		return password;
//	}
//	
//	public void setPassword(String password) {
//		this.password = password;
//	}
	
	@Override
	public String toString() {
		return "Bank [ammount=" + amount + ",\nuserName=" + userName + ",\npassword=" + password + ",\nCNIC= " + cnic +
				",\nEmail= " + email + ",\nAddress= " + address + "]";
	}
	
}
